'use strict'

function basicMultiplicaitonFunction(param1, param2){
    return new Promise((resolve, reject) =>{
        
        if(param1 && param2){
            resolve(param1 * param2)
        }else{
            reject("error")
        }
    })
}

module.exports = {
    basicMultiplicaitonFunction: async (param1, param2) => {
        let result = await basicMultiplicaitonFunction(param1, param2)
        return result
    }
}