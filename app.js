// purely a test push pull file

'use strict'
var express = require('express'),
    app     = express(),
    http    = require('http').Server(app),
    fs      = require('fs');

const basic   = require('./helpers/basicFunctions.js');

app.get('/', function(req, res){
    res.status(200).send("success, we have a running app and now we make changes")
    res.end()
});

app.get('/nextRoute', function(req, res){
    res.status(200).json({headers:JSON.stringify(req.headers)})
    res.end()
})

app.get('/basicMult', function(req, res){
    basic.basicMultiplicaitonFunction(4, 4).then((result) =>{
        res.status(200).json({result:result})
        res.end()
    }).catch(err =>{
        res.status(500).json({err: err})
        res.end()
    })
})

app.get('*', function(req, res, next) {
  var err = new Error();
  err.status = 404;
  next(err);
});
 
app.use(function(err, req, res, next) {
  if(err.status !== 404) {
    return next();
  }
 
  res.send('Oops something went wrong. We apologise for the inconvience. <br>Please go back or go to the home page <a href="/">here</a>');
});

http.listen(3000, function(){
    console.log('Started on...', 3000)
})